# Java gRPC CRUD using Gradle

This code is pretty much gRPC's java hello world example, but on
a separate repository and a bit opinionated on generated code placement.

## Contents

* Java 1.8
* Gradle 3.2.1
* gRPC-java 1.2.0


## Quick start

* Rename the package

```
NEW_PACKAGE_NAME=my.company.and.project
./rename_package.bash $NEW_PACKAGE_NAME
```

(The `rename_package.bash` script is very hacky. It also relies on
gnu-sed, so if yours is not in your `PATH` variable you can manually set
it: `SED=gsed ./rename_package.bash my.new.package`)

* Generate code and the `IDEA` configuration:

```sh
./gradlew build
./gradlew idea
```

* Import the project into `IDEA` (or just use whatever you prefer,
   actually)

* Start the server:

```sh
./gradlew runServer
# In another terminal
./gradlew runClient
```

If you want, you can also generate execution scripts and whatnot:

```sh
./gradlew installDist
```


