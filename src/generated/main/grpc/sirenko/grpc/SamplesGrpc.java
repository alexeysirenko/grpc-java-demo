package sirenko.grpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.2.0)",
    comments = "Source: sample.proto")
public final class SamplesGrpc {

  private SamplesGrpc() {}

  public static final String SERVICE_NAME = "helloworld.Samples";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<sirenko.grpc.CreateRequest,
      sirenko.grpc.CreateResponse> METHOD_CREATE_SAMPLE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "helloworld.Samples", "CreateSample"),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.CreateRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.CreateResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<sirenko.grpc.ReadRequest,
      sirenko.grpc.ReadResponse> METHOD_READ_SAMPLE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "helloworld.Samples", "ReadSample"),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.ReadRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.ReadResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<sirenko.grpc.UpdateRequest,
      sirenko.grpc.UpdateResponse> METHOD_UPDATE_SAMPLE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "helloworld.Samples", "UpdateSample"),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.UpdateRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.UpdateResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<sirenko.grpc.DeleteRequest,
      sirenko.grpc.DeleteResponse> METHOD_DELETE_SAMPLE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "helloworld.Samples", "DeleteSample"),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.DeleteRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(sirenko.grpc.DeleteResponse.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SamplesStub newStub(io.grpc.Channel channel) {
    return new SamplesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SamplesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SamplesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static SamplesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SamplesFutureStub(channel);
  }

  /**
   */
  public static abstract class SamplesImplBase implements io.grpc.BindableService {

    /**
     */
    public void createSample(sirenko.grpc.CreateRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.CreateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CREATE_SAMPLE, responseObserver);
    }

    /**
     */
    public void readSample(sirenko.grpc.ReadRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.ReadResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_READ_SAMPLE, responseObserver);
    }

    /**
     */
    public void updateSample(sirenko.grpc.UpdateRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.UpdateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_UPDATE_SAMPLE, responseObserver);
    }

    /**
     */
    public void deleteSample(sirenko.grpc.DeleteRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.DeleteResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_DELETE_SAMPLE, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_CREATE_SAMPLE,
            asyncUnaryCall(
              new MethodHandlers<
                sirenko.grpc.CreateRequest,
                sirenko.grpc.CreateResponse>(
                  this, METHODID_CREATE_SAMPLE)))
          .addMethod(
            METHOD_READ_SAMPLE,
            asyncUnaryCall(
              new MethodHandlers<
                sirenko.grpc.ReadRequest,
                sirenko.grpc.ReadResponse>(
                  this, METHODID_READ_SAMPLE)))
          .addMethod(
            METHOD_UPDATE_SAMPLE,
            asyncUnaryCall(
              new MethodHandlers<
                sirenko.grpc.UpdateRequest,
                sirenko.grpc.UpdateResponse>(
                  this, METHODID_UPDATE_SAMPLE)))
          .addMethod(
            METHOD_DELETE_SAMPLE,
            asyncUnaryCall(
              new MethodHandlers<
                sirenko.grpc.DeleteRequest,
                sirenko.grpc.DeleteResponse>(
                  this, METHODID_DELETE_SAMPLE)))
          .build();
    }
  }

  /**
   */
  public static final class SamplesStub extends io.grpc.stub.AbstractStub<SamplesStub> {
    private SamplesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SamplesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SamplesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SamplesStub(channel, callOptions);
    }

    /**
     */
    public void createSample(sirenko.grpc.CreateRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.CreateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CREATE_SAMPLE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void readSample(sirenko.grpc.ReadRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.ReadResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_READ_SAMPLE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateSample(sirenko.grpc.UpdateRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.UpdateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_UPDATE_SAMPLE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteSample(sirenko.grpc.DeleteRequest request,
        io.grpc.stub.StreamObserver<sirenko.grpc.DeleteResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_DELETE_SAMPLE, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SamplesBlockingStub extends io.grpc.stub.AbstractStub<SamplesBlockingStub> {
    private SamplesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SamplesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SamplesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SamplesBlockingStub(channel, callOptions);
    }

    /**
     */
    public sirenko.grpc.CreateResponse createSample(sirenko.grpc.CreateRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CREATE_SAMPLE, getCallOptions(), request);
    }

    /**
     */
    public sirenko.grpc.ReadResponse readSample(sirenko.grpc.ReadRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_READ_SAMPLE, getCallOptions(), request);
    }

    /**
     */
    public sirenko.grpc.UpdateResponse updateSample(sirenko.grpc.UpdateRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_UPDATE_SAMPLE, getCallOptions(), request);
    }

    /**
     */
    public sirenko.grpc.DeleteResponse deleteSample(sirenko.grpc.DeleteRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_DELETE_SAMPLE, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SamplesFutureStub extends io.grpc.stub.AbstractStub<SamplesFutureStub> {
    private SamplesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SamplesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SamplesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SamplesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<sirenko.grpc.CreateResponse> createSample(
        sirenko.grpc.CreateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CREATE_SAMPLE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<sirenko.grpc.ReadResponse> readSample(
        sirenko.grpc.ReadRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_READ_SAMPLE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<sirenko.grpc.UpdateResponse> updateSample(
        sirenko.grpc.UpdateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_UPDATE_SAMPLE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<sirenko.grpc.DeleteResponse> deleteSample(
        sirenko.grpc.DeleteRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_DELETE_SAMPLE, getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE_SAMPLE = 0;
  private static final int METHODID_READ_SAMPLE = 1;
  private static final int METHODID_UPDATE_SAMPLE = 2;
  private static final int METHODID_DELETE_SAMPLE = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SamplesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SamplesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE_SAMPLE:
          serviceImpl.createSample((sirenko.grpc.CreateRequest) request,
              (io.grpc.stub.StreamObserver<sirenko.grpc.CreateResponse>) responseObserver);
          break;
        case METHODID_READ_SAMPLE:
          serviceImpl.readSample((sirenko.grpc.ReadRequest) request,
              (io.grpc.stub.StreamObserver<sirenko.grpc.ReadResponse>) responseObserver);
          break;
        case METHODID_UPDATE_SAMPLE:
          serviceImpl.updateSample((sirenko.grpc.UpdateRequest) request,
              (io.grpc.stub.StreamObserver<sirenko.grpc.UpdateResponse>) responseObserver);
          break;
        case METHODID_DELETE_SAMPLE:
          serviceImpl.deleteSample((sirenko.grpc.DeleteRequest) request,
              (io.grpc.stub.StreamObserver<sirenko.grpc.DeleteResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class SamplesDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return sirenko.grpc.SampleProto.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SamplesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SamplesDescriptorSupplier())
              .addMethod(METHOD_CREATE_SAMPLE)
              .addMethod(METHOD_READ_SAMPLE)
              .addMethod(METHOD_UPDATE_SAMPLE)
              .addMethod(METHOD_DELETE_SAMPLE)
              .build();
        }
      }
    }
    return result;
  }
}
