// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: sample.proto

package sirenko.grpc;

public final class SampleProto {
  private SampleProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_Sample_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_Sample_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_CreateRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_CreateRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_CreateResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_CreateResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_ReadRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_ReadRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_ReadResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_ReadResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_UpdateRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_UpdateRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_UpdateResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_UpdateResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_DeleteRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_DeleteRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_helloworld_DeleteResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_helloworld_DeleteResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\014sample.proto\022\nhelloworld\"5\n\006Sample\022\n\n\002" +
      "id\030\001 \001(\t\022\t\n\001s\030\002 \001(\t\022\t\n\001i\030\003 \001(\005\022\t\n\001b\030\004 \001(" +
      "\010\"3\n\rCreateRequest\022\"\n\006sample\030\001 \001(\0132\022.hel" +
      "loworld.Sample\"4\n\016CreateResponse\022\"\n\006samp" +
      "le\030\001 \001(\0132\022.helloworld.Sample\"\031\n\013ReadRequ" +
      "est\022\n\n\002id\030\001 \001(\t\"2\n\014ReadResponse\022\"\n\006sampl" +
      "e\030\001 \001(\0132\022.helloworld.Sample\"?\n\rUpdateReq" +
      "uest\022\n\n\002id\030\001 \001(\t\022\"\n\006sample\030\002 \001(\0132\022.hello" +
      "world.Sample\"4\n\016UpdateResponse\022\"\n\006sample" +
      "\030\001 \001(\0132\022.helloworld.Sample\"\033\n\rDeleteRequ",
      "est\022\n\n\002id\030\001 \001(\t\"4\n\016DeleteResponse\022\"\n\006sam" +
      "ple\030\001 \001(\0132\022.helloworld.Sample2\247\002\n\007Sample" +
      "s\022G\n\014CreateSample\022\031.helloworld.CreateReq" +
      "uest\032\032.helloworld.CreateResponse\"\000\022A\n\nRe" +
      "adSample\022\027.helloworld.ReadRequest\032\030.hell" +
      "oworld.ReadResponse\"\000\022G\n\014UpdateSample\022\031." +
      "helloworld.UpdateRequest\032\032.helloworld.Up" +
      "dateResponse\"\000\022G\n\014DeleteSample\022\031.hellowo" +
      "rld.DeleteRequest\032\032.helloworld.DeleteRes" +
      "ponse\"\000B\035\n\014sirenko.grpcB\013SampleProtoP\001b\006",
      "proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_helloworld_Sample_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_helloworld_Sample_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_Sample_descriptor,
        new java.lang.String[] { "Id", "S", "I", "B", });
    internal_static_helloworld_CreateRequest_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_helloworld_CreateRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_CreateRequest_descriptor,
        new java.lang.String[] { "Sample", });
    internal_static_helloworld_CreateResponse_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_helloworld_CreateResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_CreateResponse_descriptor,
        new java.lang.String[] { "Sample", });
    internal_static_helloworld_ReadRequest_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_helloworld_ReadRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_ReadRequest_descriptor,
        new java.lang.String[] { "Id", });
    internal_static_helloworld_ReadResponse_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_helloworld_ReadResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_ReadResponse_descriptor,
        new java.lang.String[] { "Sample", });
    internal_static_helloworld_UpdateRequest_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_helloworld_UpdateRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_UpdateRequest_descriptor,
        new java.lang.String[] { "Id", "Sample", });
    internal_static_helloworld_UpdateResponse_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_helloworld_UpdateResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_UpdateResponse_descriptor,
        new java.lang.String[] { "Sample", });
    internal_static_helloworld_DeleteRequest_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_helloworld_DeleteRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_DeleteRequest_descriptor,
        new java.lang.String[] { "Id", });
    internal_static_helloworld_DeleteResponse_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_helloworld_DeleteResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_helloworld_DeleteResponse_descriptor,
        new java.lang.String[] { "Sample", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
