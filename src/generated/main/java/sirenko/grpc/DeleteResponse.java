// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: sample.proto

package sirenko.grpc;

/**
 * Protobuf type {@code helloworld.DeleteResponse}
 */
public  final class DeleteResponse extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:helloworld.DeleteResponse)
    DeleteResponseOrBuilder {
  // Use DeleteResponse.newBuilder() to construct.
  private DeleteResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private DeleteResponse() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private DeleteResponse(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            sirenko.grpc.Sample.Builder subBuilder = null;
            if (sample_ != null) {
              subBuilder = sample_.toBuilder();
            }
            sample_ = input.readMessage(sirenko.grpc.Sample.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(sample_);
              sample_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return sirenko.grpc.SampleProto.internal_static_helloworld_DeleteResponse_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return sirenko.grpc.SampleProto.internal_static_helloworld_DeleteResponse_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            sirenko.grpc.DeleteResponse.class, sirenko.grpc.DeleteResponse.Builder.class);
  }

  public static final int SAMPLE_FIELD_NUMBER = 1;
  private sirenko.grpc.Sample sample_;
  /**
   * <code>.helloworld.Sample sample = 1;</code>
   */
  public boolean hasSample() {
    return sample_ != null;
  }
  /**
   * <code>.helloworld.Sample sample = 1;</code>
   */
  public sirenko.grpc.Sample getSample() {
    return sample_ == null ? sirenko.grpc.Sample.getDefaultInstance() : sample_;
  }
  /**
   * <code>.helloworld.Sample sample = 1;</code>
   */
  public sirenko.grpc.SampleOrBuilder getSampleOrBuilder() {
    return getSample();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (sample_ != null) {
      output.writeMessage(1, getSample());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (sample_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getSample());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof sirenko.grpc.DeleteResponse)) {
      return super.equals(obj);
    }
    sirenko.grpc.DeleteResponse other = (sirenko.grpc.DeleteResponse) obj;

    boolean result = true;
    result = result && (hasSample() == other.hasSample());
    if (hasSample()) {
      result = result && getSample()
          .equals(other.getSample());
    }
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasSample()) {
      hash = (37 * hash) + SAMPLE_FIELD_NUMBER;
      hash = (53 * hash) + getSample().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static sirenko.grpc.DeleteResponse parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static sirenko.grpc.DeleteResponse parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static sirenko.grpc.DeleteResponse parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static sirenko.grpc.DeleteResponse parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(sirenko.grpc.DeleteResponse prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code helloworld.DeleteResponse}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:helloworld.DeleteResponse)
      sirenko.grpc.DeleteResponseOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return sirenko.grpc.SampleProto.internal_static_helloworld_DeleteResponse_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return sirenko.grpc.SampleProto.internal_static_helloworld_DeleteResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              sirenko.grpc.DeleteResponse.class, sirenko.grpc.DeleteResponse.Builder.class);
    }

    // Construct using sirenko.grpc.DeleteResponse.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      if (sampleBuilder_ == null) {
        sample_ = null;
      } else {
        sample_ = null;
        sampleBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return sirenko.grpc.SampleProto.internal_static_helloworld_DeleteResponse_descriptor;
    }

    public sirenko.grpc.DeleteResponse getDefaultInstanceForType() {
      return sirenko.grpc.DeleteResponse.getDefaultInstance();
    }

    public sirenko.grpc.DeleteResponse build() {
      sirenko.grpc.DeleteResponse result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public sirenko.grpc.DeleteResponse buildPartial() {
      sirenko.grpc.DeleteResponse result = new sirenko.grpc.DeleteResponse(this);
      if (sampleBuilder_ == null) {
        result.sample_ = sample_;
      } else {
        result.sample_ = sampleBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof sirenko.grpc.DeleteResponse) {
        return mergeFrom((sirenko.grpc.DeleteResponse)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(sirenko.grpc.DeleteResponse other) {
      if (other == sirenko.grpc.DeleteResponse.getDefaultInstance()) return this;
      if (other.hasSample()) {
        mergeSample(other.getSample());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      sirenko.grpc.DeleteResponse parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (sirenko.grpc.DeleteResponse) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private sirenko.grpc.Sample sample_ = null;
    private com.google.protobuf.SingleFieldBuilderV3<
        sirenko.grpc.Sample, sirenko.grpc.Sample.Builder, sirenko.grpc.SampleOrBuilder> sampleBuilder_;
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public boolean hasSample() {
      return sampleBuilder_ != null || sample_ != null;
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public sirenko.grpc.Sample getSample() {
      if (sampleBuilder_ == null) {
        return sample_ == null ? sirenko.grpc.Sample.getDefaultInstance() : sample_;
      } else {
        return sampleBuilder_.getMessage();
      }
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public Builder setSample(sirenko.grpc.Sample value) {
      if (sampleBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        sample_ = value;
        onChanged();
      } else {
        sampleBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public Builder setSample(
        sirenko.grpc.Sample.Builder builderForValue) {
      if (sampleBuilder_ == null) {
        sample_ = builderForValue.build();
        onChanged();
      } else {
        sampleBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public Builder mergeSample(sirenko.grpc.Sample value) {
      if (sampleBuilder_ == null) {
        if (sample_ != null) {
          sample_ =
            sirenko.grpc.Sample.newBuilder(sample_).mergeFrom(value).buildPartial();
        } else {
          sample_ = value;
        }
        onChanged();
      } else {
        sampleBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public Builder clearSample() {
      if (sampleBuilder_ == null) {
        sample_ = null;
        onChanged();
      } else {
        sample_ = null;
        sampleBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public sirenko.grpc.Sample.Builder getSampleBuilder() {
      
      onChanged();
      return getSampleFieldBuilder().getBuilder();
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    public sirenko.grpc.SampleOrBuilder getSampleOrBuilder() {
      if (sampleBuilder_ != null) {
        return sampleBuilder_.getMessageOrBuilder();
      } else {
        return sample_ == null ?
            sirenko.grpc.Sample.getDefaultInstance() : sample_;
      }
    }
    /**
     * <code>.helloworld.Sample sample = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        sirenko.grpc.Sample, sirenko.grpc.Sample.Builder, sirenko.grpc.SampleOrBuilder> 
        getSampleFieldBuilder() {
      if (sampleBuilder_ == null) {
        sampleBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            sirenko.grpc.Sample, sirenko.grpc.Sample.Builder, sirenko.grpc.SampleOrBuilder>(
                getSample(),
                getParentForChildren(),
                isClean());
        sample_ = null;
      }
      return sampleBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:helloworld.DeleteResponse)
  }

  // @@protoc_insertion_point(class_scope:helloworld.DeleteResponse)
  private static final sirenko.grpc.DeleteResponse DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new sirenko.grpc.DeleteResponse();
  }

  public static sirenko.grpc.DeleteResponse getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<DeleteResponse>
      PARSER = new com.google.protobuf.AbstractParser<DeleteResponse>() {
    public DeleteResponse parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new DeleteResponse(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<DeleteResponse> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<DeleteResponse> getParserForType() {
    return PARSER;
  }

  public sirenko.grpc.DeleteResponse getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

