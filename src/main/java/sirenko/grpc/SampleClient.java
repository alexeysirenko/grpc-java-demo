package sirenko.grpc;


import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SampleClient {
    private static final Logger logger = Logger.getLogger(SampleClient.class.getName());

    private final ManagedChannel channel;
    private SamplesGrpc.SamplesBlockingStub blockingStub;

    public SampleClient(String hostname, int port) {
        channel = ManagedChannelBuilder.forAddress(hostname, port)
                .usePlaintext(true)
                .build();
        blockingStub = SamplesGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public void test() {
        logger.info("Trying to test a server");
        try {
            Sample sample = Sample.newBuilder()
                    .setI(42)
                    .setS("Eggs")
                    .setB(true)
                    .build();

            Sample createdSample = create(sample);
            Sample readSample = read(createdSample.getId());

            Sample modifiedSample = readSample.toBuilder()
                    .setS("Bacon")
                    .build();
            Sample updatedSample = update(readSample.getId(), modifiedSample);

            delete(updatedSample.getId());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "Request to grpc server failed", e);
        }
    }

    private Sample create(Sample sample) {
        CreateRequest request = CreateRequest.newBuilder()
                .setSample(sample)
                .build();
        CreateResponse response = blockingStub.createSample(request);
        logger.info("Create response: " + response.getSample());
        return response.getSample();
    }

    private Sample read(String id) {
        ReadRequest request = ReadRequest.newBuilder()
                .setId(id)
                .build();
        ReadResponse response = blockingStub.readSample(request);
        logger.info("Read response: " + response.getSample());
        return response.getSample();
    }

    private Sample update(String id, Sample sample) {
        UpdateRequest request = UpdateRequest.newBuilder()
                .setId(id)
                .setSample(sample)
                .build();
        UpdateResponse response = blockingStub.updateSample(request);
        logger.info("Update response: " + response.getSample());
        return response.getSample();
    }

    private Sample delete(String id) {
        DeleteRequest request = DeleteRequest.newBuilder()
                .setId(id)
                .build();
        DeleteResponse response = blockingStub.deleteSample(request);
        logger.info("Delete response: " + response.getSample());
        return response.getSample();
    }



    public static void main(String[] args) throws Exception {
        SampleClient client = new SampleClient("localhost", 42420);
        try {
            client.test();
        } finally {
            client.shutdown();
        }
    }
}
