package sirenko.grpc;


import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class SampleServer {

    private static final Logger logger = Logger.getLogger(SampleServer.class.getName());

    private int port = 42420;
    private Server server;

    private void start() throws Exception {
        logger.info("Starting the grpc server");

        server = ServerBuilder.forPort(port)
                .addService(new SamplesImpl())
                .build()
                .start();

        logger.info("Server started. Listening on port " + port);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.err.println("*** JVM is shutting down. Turning off grpc server as well ***");
            SampleServer.this.stop();
            System.err.println("*** shutdown complete ***");
        }));
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }


    public static void main(String[] args) throws Exception {
        logger.info("Server startup. Args = " + Arrays.toString(args));
        final SampleServer sampleServer = new SampleServer();

        sampleServer.start();
        sampleServer.blockUntilShutdown();
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    private class SamplesImpl extends SamplesGrpc.SamplesImplBase {

        private final Map<String, Sample> samples = new ConcurrentHashMap<>();

        @Override
        public void createSample(CreateRequest request, StreamObserver<CreateResponse> responseObserver) {
            String id = UUID.randomUUID().toString();
            Sample sample = request.getSample().toBuilder().setId(id).build();
            samples.put(id, sample);
            CreateResponse response = CreateResponse.newBuilder().setSample(sample).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }

        @Override
        public void readSample(ReadRequest request, StreamObserver<ReadResponse> responseObserver) {
            String id = request.getId();
            Sample sample = samples.get(id);
            if (sample != null) {
                ReadResponse response = ReadResponse.newBuilder().setSample(sample).build();
                responseObserver.onNext(response);
            } else {
                responseObserver.onError(new Exception(String.format("Sample with id %s does not exist", id)));
            }
            responseObserver.onCompleted();
        }

        @Override
        public void updateSample(UpdateRequest request, StreamObserver<UpdateResponse> responseObserver) {
            String id = request.getId();
            Sample sample = request.getSample();
            samples.put(id, sample);
            UpdateResponse response = UpdateResponse.newBuilder().setSample(sample).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }

        @Override
        public void deleteSample(DeleteRequest request, StreamObserver<DeleteResponse> responseObserver) {
            String id = request.getId();
            Sample sample = samples.get(id);
            if (sample != null) {
                DeleteResponse response = DeleteResponse.newBuilder().setSample(sample).build();
                responseObserver.onNext(response);
            } else {
                responseObserver.onError(new Exception(String.format("Sample with id %s does not exist", id)));
            }
            responseObserver.onCompleted();
        }
    }
}
